# ics-ans-role-elastictest

Ansible role to install elastictest.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-elastictest
```

## License

BSD 2-clause
